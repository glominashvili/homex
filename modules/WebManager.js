var hapi = require('hapi');
var path = require('path');

var WebManager = function() {
    var that = this;
    this.server = new hapi.Server();
    this.routes = [{
        method: 'GET',
        path: '/test',
        handler: function(request, reply) {

            reply('Testing');

        }
    }, {
        method: 'GET',
        path: '/{param*}',
        handler: {
            directory: {
                path: path.join(__dirname, '../public')
            }
        }
    }];

    this.server.connection({
        port: 8888
    });

    this.routes.forEach(function(el) {
        that.server.route(el);
    });

    this.start = function() {
        that.server.start();
    };
    
    this.addModulePublic = function(moduleId, modulePath){
        var route = {
            method: 'GET',
            path: '/module/'+moduleId+'/{param*}',
            handler: {
                directory: {
                    path: path.join(modulePath, '/public')
                }
            }
        };
        that.server.route(route);
    };

};

module.exports = WebManager;
