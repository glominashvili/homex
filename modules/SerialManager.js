var util = require('util');
var EventEmitter = require('events').EventEmitter;
var SerialPort = require("serialport").SerialPort;
var serialport = require('serialport');

var SerialManager = function(eventEmmiter) {
    var that = this;
    this.buffer = "";
    this.serialPort = new SerialPort("/dev/ttyACM0", {
        baudrate: 9600,
        parser: serialport.parsers.readline("\n")
    });

    this.serialPort.on("open", function() {
        console.log('serialport open');
        that.serialPort.on('data', function(data) {
            console.log("Serial Data " + data);
            if(data.length > 5){
                that.emit('data', data.toString('utf-8'));
            }
        });

    });

};

util.inherits(SerialManager, EventEmitter);

module.exports = SerialManager;
