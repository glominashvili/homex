var FakeSerial = require('./FakeSerial');
var EventEmitter = require('events').EventEmitter;
var ModuleManager = require('./ModuleManager');
var SocketManager = require('./SocketManager');
var WebManager = require('./WebManager');
var DB = require('./DB');
var SerialManager = require('./SerialManager');

var HomexCore = function() {
    var that = this;
    this.fakeSerial = new FakeSerial();
    this.eventEmitter = new EventEmitter();
    this.moduleManager = new ModuleManager();
    this.webManager = new WebManager();
    this.socketManager = new SocketManager();
    this.serialManager = new SerialManager();
    this.webManager.start();
    this.socketManager.start();
    this.socketManager.on('connection', function() {
        console.log('core knows!');
    });
    //Code to make fake serial connection
    //setInterval(this.fakeSerial.emitRandomData, 5000);
    //setInterval(this.fakeSerial.emitNewSensor, 10000);

/*    this.fakeSerial.on('data', function(sdata){*/
        //console.log(sdata); 
        //that.moduleManager.emit('recSerialData', sdata);
        //that.socketManager.send(sdata);
    //});

    this.serialManager.on('data', function(data){
        that.moduleManager.emit('recSerialData', data);
        that.socketManager.send(data);
    });

    //this.fakeSerial.on('newSensor', console.log);
    //this.moduleManager.on('sendSerialData', this.fakeSerial.send);
    this.moduleManager.on('sendSocketData', this.socketManager.send);
    this.moduleManager.on('installModule', this.webManager.addModulePublic);
    this.moduleManager.on('installModule', function(moduleId, path){
        that.socketManager.send({
            cmd: 'installModule',
            moduleId: moduleId 
        });
    });
    this.moduleManager.on('removeModule', function(moduleId){
        that.socketManager.send({
            cmd: 'removeModule',
            moduleId: moduleId 
        });
    });

    this.socketManager.on('connection', function(connId){
        that.moduleManager.modules.forEach(function(module, index){
            that.socketManager.send({
                cmd: 'installModule',
                moduleId: index 
            }, connId);            
        });
    });

};

module.exports = HomexCore;
