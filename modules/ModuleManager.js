var util = require('util');
var EventEmitter = require('events').EventEmitter;
var chokidar = require('chokidar');
//var path = require('path');
//var fs = require('fs');

var ModuleManager = function() {

    var that = this;
    this.modules = [];
    this.fs = require('fs');
    this.path = require('path');
    this.modulesDir = this.path.join(__dirname, '../hmodules/');


    this.newModuleHandler = function(dpath) {

        try {
            var moduleId = that.modules.length;
            var moduleObj = require(that.path.join(dpath, '/index'));
            var module = {
                obj: new moduleObj(),
                info: '',
                path: dpath
            };

            module.obj.on('sendSocketData', function(socketData) {
                that.emit('sendSocketData', socketData, moduleId, dpath, module.info);
            });

            module.obj.on('sendSerialData', function(serialData) {
                that.emit('sendSerialData', serialData, moduleId, dpath, module.info);
            });

            that.modules[moduleId] = module;
            console.log("installing module with id:"+moduleId);
            that.emit('installModule', moduleId, dpath, module.info);
        } catch (ex) {
            if (typeof ex === 'object') {
                if (ex.message) {
                    console.log('\nMessage: ' + ex.message);
                }
                if (ex.stack) {
                    console.log('\nStacktrace:');
                    console.log('====================');
                    console.log(ex.stack);
                }
            } else {
                console.log('dumpError :: argument is not an object');
            }

        }


    };

    this.watcher = chokidar.watch(this.modulesDir, {});

    this.watcher.on('addDir', function(dpath) {
        if (dpath === that.modulesDir)
            return;

        console.log(dpath);
	setTimeout(function() {
	    that.fs.exists(that.path.join(dpath, '/package.json'), function(exists) {
            if (exists) {
                that.newModuleHandler(dpath);
            } else {
                console.log('pakage json doest exsists');
            }
        });
	}, 3000);

    });

    this.watcher.on('unlinkDir', function(dpath) {

        that.modules.forEach(function(module, index) {
            if (module.path === dpath) {
                module.obj.deconstruct();
                console.log("removing module!");
                delete that.modules[index];
                that.emit('removeModule', index);
            }
        });

    });

    this.watcher.on('add', function(dpath) {

    });

    this.on('recSocketData', function(socketData) {
        that.modules.forEach(function(module) {
            try {
                module.obj.emit('recSocketData', socketData);
            } catch (ex) {
                console.log("can't emitt socket data for module");
            }
        });
    });

    this.on('recSerialData', function(serialData) {
        that.modules.forEach(function(module) {
            try {
                module.obj.emit('recSerialData', serialData);
            } catch (ex) {
                console.log("can't emitt socket data for module");
            }
        });
    });

};

util.inherits(ModuleManager, EventEmitter);

module.exports = ModuleManager;
